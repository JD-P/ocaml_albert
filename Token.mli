type t =
  | LCBRACE (* { *)
  | RCBRACE (* } *)
  | LSBRACKET (* [ *)
  | RSBRACKET (* ] *)
  | LPAREN (* ( *)
  | RPAREN (* ) *)
  | EQ (* = *)
  | COLON (* : *)
  | SEMICOLON (* ; *)
  | IMPLIES (* -> *)
  | PLUS (* + *)
  | MINUS (* - *)
  (* OCaml Style Comments *)
  | LCOMMENT (* \(\* *)
  | RCOMMENT (* \*\) *)
  | EOF
  | Ident of string
  | Int of int
  | Def
  | Drop
      

type token = t

val to_string: t -> string
