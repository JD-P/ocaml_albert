let _ =
  try
    let lexbuf = Lexing.from_channel stdin in
    while true do
      let result = Token.to_string (Lexer.scan lexbuf) in
      if (result = "End of File") then (print_string result; exit 0)
      else
      print_string result; print_newline(); flush stdout
    done
  with Lexer.Eof -> exit 0
