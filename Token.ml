type t =
  | LCBRACE
  | RCBRACE
  | LSBRACKET
  | RSBRACKET
  | LPAREN
  | RPAREN
  | EQ
  | COLON
  | SEMICOLON
  | IMPLIES
  | PLUS
  | MINUS
  | LCOMMENT
  | RCOMMENT
  | EOF
  | Ident of string
  | Int of int
  | Def
  | Drop
      
type token = t

let to_string = function
  | LCBRACE -> "{"
  | RCBRACE -> "}"
  | LSBRACKET -> "["
  | RSBRACKET -> "]"
  | LPAREN -> "("
  | RPAREN -> ")"
  | EQ -> "="
  | COLON -> ":"
  | SEMICOLON -> ";"
  | IMPLIES -> "->"
  | PLUS -> "+"
  | MINUS -> "-"
  | LCOMMENT -> "(*"
  | RCOMMENT -> "*)"
  | EOF -> "End of File"
  | Ident id -> "Ident" (* TODO: Include actual token text *)
  | Int num -> "Integer"
  | Def -> "def"
  | Drop -> "drop"
