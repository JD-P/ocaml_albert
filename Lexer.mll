{

module Keywords = Map.Make(String)

let keyword_list = [
  ("def", Token.Def);
  ("drop", Token.Drop)
]

exception NonPair of string

let keyword_map =
  let add_pair
      (kwd_map: Token.t Map.Make(String).t)
      (pair: (Keywords.key * Token.t)) =
    match pair with
    | (a, b) -> Keywords.add a b kwd_map
    | _ -> raise (NonPair "add_pair given a non-pair argument")
  in List.fold_left add_pair Keywords.empty keyword_list


let reset_file ~file buffer =
  let open Lexing in
  buffer.lex_curr_p <- {buffer.lex_curr_p with pos_fname = file}

let reset_line ~line buffer =
  let open Lexing in
  buffer.lex_curr_p <- {buffer.lex_curr_p with pos_lnum = line}

let reset_offset ~offset buffer =
  assert (offset >= 0);
  let open Lexing in
  let bol = buffer.lex_curr_p.pos_bol in
  buffer.lex_curr_p <- {buffer.lex_curr_p with pos_cnum = bol + offset }

let reset ?file ?line ?offset buffer =
  let () =
    match file with
      Some file -> reset_file ~file buffer
    |      None -> () in
  let () =
    match line with
      Some line -> reset_line ~line buffer
    |      None -> () in
  match offset with
    Some offset -> reset_offset ~offset buffer
  |        None -> ()


exception Eof

}

let newline = ['\n' '\r']
let white = [' ' '\t']
let digit = ['0'-'9']
let natnum = digit | digit (digit | '_')* digit
let integer = '-'? natnum
let decimal = digit+ '.' digit+

let small = ['a'-'z']
let capital = ['A'-'Z']
let letter = small | capital

let ichar = letter | digit | ['_' '\'']
let ident    = small ichar* | '_' ichar+
               
rule scan =
  parse newline { Lexing.new_line lexbuf; scan lexbuf } 
      | white+ {scan lexbuf }
      | "{"  { print_string("Test") ; Token.LCBRACE }
      | "}"  { Token.RCBRACE }
      | "["  { Token.LSBRACKET }
      | "]"  { Token.RSBRACKET }
      | "("  { Token.LPAREN }
      | ")"  { Token.RPAREN }
      | "="  { Token.EQ }
      | ":"  { Token.COLON }
      | ";"  { Token.SEMICOLON }
      | "->" { Token.IMPLIES }
      | "+"  { Token.PLUS }
      | "-"  { Token.MINUS }
      | eof  { Token.EOF }
      | integer as n { Token.Int (int_of_string n) }
      | ident as var {
          match Keywords.find var keyword_map with
          | exception Not_found -> Token.Ident var
          | kwd -> kwd
        }
      | "*)" { Token.RCOMMENT }
      | "(*" { let rec strip_comment buffer = 
                 match (scan buffer) with
                 | Token.RCOMMENT -> scan buffer
                 | Token.EOF -> raise Eof
                 | _ -> (strip_comment buffer)
               in strip_comment lexbuf
             }
          
{


}
